<!DOCTYPE html>
<html>
<head>
	<title>Jadwal Asisten</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
	<link rel="stylesheet" type="text/css" href="<?=base_url('assets/css/style.css');?>">
</head>
<body>
	

	<div class="container jadwal_asisten">
		<center><p>Jadwal Asisten</p></center>
		<a href="<?= base_url('login/logout') ?>">
				<button class="btn btn-danger">Log Out</button>
		</a>
		
		<table class="table">
  			<thead>
    			<tr>
     				<!-- <th scope="col">ID Jadwal</th> -->
      				<th scope="col">Nama Asisten</th>
      				<th scope="col">Ruang</th>
      				<th scope="col">Hari</th>
      				<th scope="col">Waktu</th>
      				<th scope="col">Opsi</th>
    			</tr>
  			</thead>
			<tbody>
				<?php foreach ($asisten as $asisten): ?>
					<tr>
						<!-- <td><?= $asisten->id_jadwal;?></td> -->
						<td><?= $asisten->nama;?></td>
						<td><?= $asisten->lab;?></td>
						<td><?= $asisten->hari;?></td>
						<td><?= $asisten->waktu;?></td>
						<td>
							<a href="<?=base_url('jadwal/delete/' . $asisten->id_jadwal);?>" class="btn btn-danger">Hapus</a>
							<a href="<?=base_url('jadwal/editData/' . $asisten->id_jadwal);?>" class="btn btn-warning">Edit</a>
						</td>
						
					</tr>
				<?php endforeach ?>
			</tbody>
		</table>
			<center>
				<a href="<?=base_url('jadwal/addData');?>" class="btn btn-primary">Tambah Jadwal</a>
			</center>		
	
	</div>

</body>
</html>