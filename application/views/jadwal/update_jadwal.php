<!DOCTYPE html>
<html>
<head>
	<title>Tambah Jadwal</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
	<link rel="stylesheet" type="text/css" href="<?=base_url('assets/css/style.css');?>">
</head>
<body>
<div class="container update">
	<a href="<?= base_url('login/logout') ?>">
				<button class="btn btn-danger">Log Out</button>
	</a>
	<form method="post" action="<?=base_url('jadwal/update/'. $jadwal->id_jadwal)?>" class="form">
		<div class="container">
			<div class="form-group row">
	    		<label class="col-sm-2 col-form-label">Nama Asisten</label>
	    		<div class="col-sm-10">
	    			<select class="custom-select" name="id_asisten">
				  		<option selected value="<?=$jadwal->id_asisten?>">
				  			<?=$jadwal->nama?>
				  		</option>

	    				<?php foreach ($asisten as $asisten): ?>
				  		<option value="<?=$asisten->id?>">
				  			<?= $asisten->nama;?>
				  		</option>
				  		<?php endforeach ?>

					</select>
	    		</div>
	    	</div>
  		<!-- </div> -->

  		<!-- <div class="container"> -->
			<div class="form-group row">
	    		<label class="col-sm-2 col-form-label">Nama Lab</label>
	    		<div class="col-sm-10">
	    			<select class="custom-select" name="lab">
	    				<option selected value="<?=$jadwal->lab?>"><?=$jadwal->lab?></option>
						<option value="Komputasi">Lab Komputasi</option>
						<option value="Jaringan">Lab Jaringan</option>
						<option value="Basis Data">Lab Basis Data</option>
					</select>
	    		</div>
	    	</div>
  		<!-- </div> -->

  		<!-- <div class="container"> -->
			<div class="form-group row">
	    		<label class="col-sm-2 col-form-label">Hari</label>
	    		<div class="col-sm-10">
	    			<select class="custom-select" name="hari">
	    				<option selected value="<?=$jadwal->hari?>">Senin</option>
						<option value="Senin">Senin</option>
						<option value="Selasa">Selasa</option>
						<option value="Rabu">Rabu</option>
						<option value="Kamis">Kamis</option>
						<option value="Jumat">Jum'at</option>
					</select>
	    		</div>
	    	</div>
  		<!-- </div> -->


  		<!-- <div class="container"> -->
			<div class="form-group row">
	    		<label class="col-sm-2 col-form-label">Waktu</label>
	    		<div class="col-sm-10">
	    			<select class="custom-select" name="waktu">
	    				<option selected value="<?=$jadwal->waktu?>"><?=$jadwal->waktu?></option>
						<option value="8.00 - 10.00">8.00 - 10.00</option>
						<option value="10.00 - 12.00">10.00 - 12.00</option>
						<option value="13.00 - 15.00">13.00 - 15.00</option>
					</select>
	    		</div>
	    	<!-- </div> -->

  		</div>
  		
			<button type="submit" class="btn btn-primary">Perbarui</button>
		
	</form>
</div>
</body>
</html>