<!DOCTYPE html>
<html>
<head>
	<title>Data Asisten</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
	<link rel="stylesheet" type="text/css" href="<?=base_url('assets/css/style.css');?>">
</head>
<body>
	<div class="container data_asisten">
		<a href="<?= base_url('login/logout') ?>">
				<button class="btn btn-danger">Log Out</button>
		</a>
		<table class="table">
			 <p style="color: red;"><?= $this->session->flashdata('pesan') ?></p>
  			<thead>
    			<tr>
     				<th scope="col">ID</th>
      				<th scope="col">Nama</th>
      				<th scope="col">NIM</th>
    			</tr>
  			</thead>
			<tbody>
				<?php foreach ($asisten as $asisten): ?>
					<tr>
						<td><?= $asisten->id;?></td>
						<td><?= $asisten->nama;?></td>
						<td><?= $asisten->nim;?></td>
					</tr>
				<?php endforeach ?>
			</tbody>
		</table>
			
			<a href="<?=base_url('jadwal')?>">
				<button class="btn btn-primary">Lihat Jadwal</button>
			</a>
	</div>

</body>
</html>