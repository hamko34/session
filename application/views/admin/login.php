<!DOCTYPE html>
<html lang="en">

<head>
    <title>Login</title>
    <!-- base_url() = localhost/pelatihan_itc/assets/css/bootstrap.min.css -->
   <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
   <link rel="stylesheet" type="text/css" href="<?=base_url('assets/css/style.css');?>">
</head>

<body>
<center>        
<div class="container login">
    <h2>Login Admin</h2>
    <p style="color: red;"><?= $this->session->flashdata('pesan') ?></p>
    <form action="<?= base_url('login/cek') ?>" method="post">
        <table>
            <tr>
                <td>Username</td>
                <td> : </td>
                <td><input type="text" name="username" /></td>
            </tr>
            <tr>
                <td>Password</td>
                <td> : </td>
                <td><input type="password" name="password" /></td>
            </tr>
            <tr>
                <td colspan="2">
                    <input type="submit" name="login" value="Login" 
                    class="btn btn-primary" />
                </td>
            </tr>
        </table>
    </form>
</div>
</center>

</body>

</html>