<?php 
	
	class Asisten extends CI_Controller
	{
		public function __construct()
		{
			parent :: __construct();
			$this->load->model('asisten_model');
			if (!$this->session->userdata('username')) 
			{
				$this->session->set_flashdata('pesan'
                ,'<div style="color: red"><i><b>Kamu Harus Login</b></i></div>');
				redirect('login');	
			}
		}
		
		public function index()
		{
			$data['asisten'] = $this->asisten_model->getData();
			$this->load->view('asisten/data_asisten',$data);
		}

	}
 ?>