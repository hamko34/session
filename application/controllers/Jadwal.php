<?php 
	
	class Jadwal extends CI_Controller
	{
		public function __construct()
		{
			parent :: __construct();
			$this->load->model('jadwal_model');
			if (!$this->session->userdata('username')) 
			{
				$this->session->set_flashdata('pesan'
                ,'<div style="color: red"><i><b>Kamu Harus Login</b></i></div>');
				redirect('login');	
			}
		}
		
		public function index()
		{
			$data['asisten'] = $this->jadwal_model->getData();
			$this->load->view('jadwal/jadwal_asisten',$data);
		}

		public function delete($id_jadwal)
		{
			$this->jadwal_model->delete($id_jadwal);
			redirect(base_url('jadwal'));
		}

		public function addData()
		{
			$data['asisten'] = $this->jadwal_model->getDataAsisten();
			$this->load->view('jadwal/tambah_jadwal',$data);
		}

		public function insert()
		{
			$hari = $this->input->post('hari');
			$id_jadwal = $this->input->post('id_jadwal');
			$id_asisten = $this->input->post('id_asisten');
			$waktu = $this->input->post('waktu');
			$lab = $this->input->post('lab');

			$data = array(
					'hari'=> $hari,
					'id_jadwal' => $id_jadwal,
					'id_asisten' => $id_asisten,
					'waktu' => $waktu,
					'lab' => $lab 
			);

			$this->jadwal_model->insertData($data);
			redirect('jadwal');
		}

		public function editData($id_jadwal)
		{
			$data['jadwal'] = $this->jadwal_model->getDataJadwal($id_jadwal);
			$data['asisten'] = $this->jadwal_model->getDataAsisten();
			$this->load->view('jadwal/update_jadwal',$data);
		}

		public function update($id_jadwal)
		{
			$nama = $this->input->post('id_asisten');
			$lab = $this->input->post('lab');
			$hari = $this->input->post('hari');
			$waktu = $this->input->post('waktu');

			$data = array(
					'id_jadwal' => $id_jadwal,
					'id_asisten' => $nama,
					'lab' => $lab,
					'hari' => $hari,
					'waktu' => $waktu
			);

			$this->jadwal_model->updateData($data);
			redirect('jadwal');

		}



	}
 ?>