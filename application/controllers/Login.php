<?php

class Login extends CI_Controller
{

    public function __construct()
    {
        parent:: __construct();
        $this->load->model('login_model');
        //$this->load->library('session');
    }

    public function index()
    {
        $this->load->view('admin/login');
    }
     public function cek()
    {
        if(isset($_POST['login']))
        {
            $username = $this->input->post('username');
            $password = $this->input->post('password');
            $data = array
            (
                'username' => $username,
                'password' => $password
            );
            $cek = $this->login_model->cekLogin("admin",$data);
            if($cek) // jika berhasil login
            {

                //$_SESSION['username'] = $username;
                $this->session->set_userdata(array(
                    'username' => $username
                ));

                $this->session->set_flashdata('pesan'
                ,'<div style="color: green"><i><b>Sukses Login</b></i></div>');

                redirect('asisten');
            }
            else
            {
                $this->session->set_flashdata('pesan'
                ,'<div style="color: red"><i><b>Gagal Login</b></i></div>');

                redirect('login');
            }

        }
    }

    public function logout()
    {
        $this->session->unset_userdata('username');
        //$this->session->sess_destroy();

        
        // $this->load->view('v_login');
        redirect('login?pesan=Berhasil Log Out');
    }

}

?>
