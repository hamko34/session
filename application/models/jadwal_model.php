<?php
	/**
	 * 
	 */
	class jadwal_model extends CI_Model
	{
		
		const TABLE_NAME = "jadwal_asisten";

		public function getData()
		{
			$query = $this->db->select('*')
							  ->from('jadwal_asisten ja')
							  ->join('data_asisten','data_asisten.id = ja.id_asisten')
							  ->get()->result();
			return $query;
		}

		public function delete($id_jadwal)
		{
			$query = $this->db->where('id_jadwal',$id_jadwal)
							  ->delete($this::TABLE_NAME);

		}

		public function insertData($data)
		{
			$this->db->insert('jadwal_asisten',$data);
		}

		public function getDataAsisten()
		{
			$query = $this->db->select('*')->get('data_asisten')->result();

			return $query;
		}

		public function getDataJadwal($id_jadwal)
		{
			$query = $this->db->select('*')
						      ->from('data_asisten')
						      ->join('jadwal_asisten','jadwal_asisten.id_asisten = data_asisten.id')
						      ->where('id_jadwal',$id_jadwal)
						      ->get()
						      ->row();

			return $query;
		}

		public function updateData($data)
		{
			 $this->db->replace('jadwal_asisten',$data);
		}

	}