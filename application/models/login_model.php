<?php 
	class login_model extends CI_Model
	{
		const TABLE_NAME = "admin";

		public function getData()
		{
			$query = $this->db->select('*')
							  ->get($this::TABLE_NAME)->result();
			return $query;
		}

		public function cekLogin($admin,$data)
		{
			$user = $this->db
					 ->get_where($admin,$data)
					 ->row();
			if(!$user)
			{ return false; }
			else
			return $user;
		}
	}

 ?>