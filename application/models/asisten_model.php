<?php
	/**
	 * 
	 */
	class asisten_model extends CI_Model
	{
		
		const TABLE_NAME = "data_asisten";

		public function getData()
		{
			$query = $this->db->select('*')
							  ->get($this::TABLE_NAME)->result();
			return $query;
		}
	}