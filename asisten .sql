-- phpMyAdmin SQL Dump
-- version 4.8.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 16, 2019 at 06:09 PM
-- Server version: 10.1.33-MariaDB
-- PHP Version: 7.2.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `asisten`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `id` int(10) NOT NULL,
  `username` varchar(30) NOT NULL,
  `password` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`id`, `username`, `password`) VALUES
(1, 'aku', 'kamu'),
(2, 'dia', 'dia'),
(3, 'ilham', 'ilham');

-- --------------------------------------------------------

--
-- Table structure for table `data_asisten`
--

CREATE TABLE `data_asisten` (
  `id` int(30) NOT NULL,
  `nama` varchar(30) NOT NULL,
  `nim` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `data_asisten`
--

INSERT INTO `data_asisten` (`id`, `nama`, `nim`) VALUES
(1, 'ilham', '123'),
(2, 'habib', '124'),
(3, 'ravi', '125'),
(4, 'rahmat', '12311');

-- --------------------------------------------------------

--
-- Table structure for table `jadwal_asisten`
--

CREATE TABLE `jadwal_asisten` (
  `id_jadwal` int(30) NOT NULL,
  `id_asisten` int(30) NOT NULL,
  `lab` varchar(50) NOT NULL,
  `hari` varchar(10) NOT NULL,
  `waktu` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `jadwal_asisten`
--

INSERT INTO `jadwal_asisten` (`id_jadwal`, `id_asisten`, `lab`, `hari`, `waktu`) VALUES
(10, 1, 'Lab Komputasi', 'Senin', '8.00 - 10.00'),
(11, 2, 'Lab Jaringan', 'Senin', '10.00 - 12.00'),
(12, 3, 'Lab Komputasi', 'Selasa', '13.00 - 15.00'),
(13, 1, 'Lab Jaringan', 'Selasa', '13.00 - 15.00'),
(16, 4, 'Lab Jaringan', 'Rabu', '8.00 - 10.00'),
(22, 2, 'Lab Komputasi', 'Jumat', '8.00 - 10.00'),
(24, 3, 'Lab Komputasi', 'Rabu', '13.00 - 15.00'),
(25, 3, 'Lab Jaringan', 'Rabu', '10.00 - 12.00');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `data_asisten`
--
ALTER TABLE `data_asisten`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `jadwal_asisten`
--
ALTER TABLE `jadwal_asisten`
  ADD PRIMARY KEY (`id_jadwal`),
  ADD KEY `fk_idasisten` (`id_asisten`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `data_asisten`
--
ALTER TABLE `data_asisten`
  MODIFY `id` int(30) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `jadwal_asisten`
--
ALTER TABLE `jadwal_asisten`
  MODIFY `id_jadwal` int(30) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `jadwal_asisten`
--
ALTER TABLE `jadwal_asisten`
  ADD CONSTRAINT `fk_idasisten` FOREIGN KEY (`id_asisten`) REFERENCES `data_asisten` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
